#include <stdio.h>

int main()
{
    int pilihMenu;
    float inputNumber;
    float hasil = 0;
    
    printf("====================================\n");
    printf("Program Kalkulator Sederhana\n");
    printf("====================================\n\n");
    printf("Masukan Angka : ");
    scanf("%f",&hasil);

    printf("Pilih Menu\n ");
    printf("1. Tambah (+)\n ");
    printf("2. Kurang (-)\n ");
    printf("3. Kali (*)\n ");
    printf("4. Bagi (/)\n ");
    printf("5. Modulus (\%) \n ");
    printf("6. Get Result\n ");
    
    printf("Pilih Menu : ");
    scanf("%d",&pilihMenu);
    
    
    while(pilihMenu != 6) {
        switch (pilihMenu) {
            case 1: 
            printf("Anda Memilih Operator Penjumlahan \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil + inputNumber;
            break;
            
            case 2: 
            printf("Anda Memilih Operator Pengurangan \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil - inputNumber;
            break;
            
            case 3: 
            printf("Anda Memilih Operator Perkalian \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil * inputNumber;
            break;
            
            case 4: 
            printf("Anda Memilih Operator Pembagian \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = hasil / inputNumber;
            break;

            case 5: 
            printf("Anda Memilih Operator Modulus \n");
            printf("Masukan angka: ");
            scanf("%f",&inputNumber);
            hasil = (int)hasil % (int)inputNumber;
            break;
        }
        printf("Pilih Menu : ");
        scanf("%d",&pilihMenu);
    }
    
    printf("-------------------------------\n");
    printf("hasil: %0.2f", hasil);
    printf("\n-------------------------------\n");

    return 0;
}