#include <stdio.h>
#include <math.h>


void main(){
    int x;
    float hasil;
    
    printf("Formula P(x) = x^4 + 7x^3 - 5x + 9   \n\n");
    printf("Masukkan nilai x = ");
    scanf("%d", &x);
    
    hasil=(pow(x,4))+(7*pow(x,3)-(5*x))+9;
    
    printf("Didapatkan hasil = %2.f", hasil);
}